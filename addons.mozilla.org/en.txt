⚠ This addon extends and works only with <a href="https://addons.mozilla.org/firefox/addon/tree-style-tab/">Tree Style Tab</a>.

Clicking active tab activate the last active one. It allows you to switch quickly between two tabs without moving the mouse, very useful when the two tabs are far away.

You can also use Shift+F2 to trigger the tab flip.

It was made to work well with Multiple Tab Handler.
